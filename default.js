$(document).ready(function(){

document.getElementById('typeselect').addEventListener('change', typeSelected);

function typeSelected() {
   if (this.value === 'disk') {
       document.getElementById("disks").removeAttribute("hidden");
       document.getElementById("books").setAttribute("hidden", "hidden");
       document.getElementById("furnitures").setAttribute("hidden", "hidden");
   }
   if (this.value === 'book') {
       document.getElementById("books").removeAttribute("hidden");
       document.getElementById("disks").setAttribute("hidden", "hidden");
       document.getElementById("furnitures").setAttribute("hidden", "hidden");
   }
   if (this.value === 'furniture') {
       document.getElementById("furnitures").removeAttribute("hidden");
       document.getElementById("disks").setAttribute("hidden", "hidden");
       document.getElementById("books").setAttribute("hidden", "hidden");
   }
}
});



