<?php

use App\Core\App;

App::bind('config', require 'Config.php');

App::bind('database',
    new QueryBuilder(
    Connection::make(App::get('config')['database'])
));

/**
 *Renders view and provides product data
 *
 * @param string $name 
 * @param array $data 
 */
function view($name, $data = [])
{
    extract($data);


    require "app/views/partials/header.php";
    require "app/views/{$name}_view.php";
}

/**
 *
 * @param string $path Returns URL.
 */
function redirect($path)
{
    header("Location:/JuniorTest/{$path}");
}
