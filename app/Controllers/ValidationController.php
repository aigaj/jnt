<?php

namespace App\Controllers;

/**
 * Class responsible for data validation
 * views the completed fields and checks if data is valid
 * if field(-s) are not suitable with defined rules shows an error
 *
 */
class ValidationController extends Controller
{

    /**
     *
     * @param array $data
     */
    public function rules($data)
    {
        foreach ($data as $key => $value) {

            foreach ($value as $rule => $error) {

                switch ($rule) {

                    case 'numeric':
                        if (!is_numeric($_POST[$key]) == $key) {
                            $this->message[$key] = $error;
                        }
                        break;

                    case 'alphanumeric':
                        if (!ctype_alnum($_POST[$key]) == $key) {
                            $this->message[$key] = $error;
                        }
                        break;

                    case 'between':
                        foreach ($error as $between => $minmax) {

                            switch ($between) {
                                case 'min':

                                    if (strlen($_POST[$key]) < $minmax) {
                                        $this->message[$key] = $error['error'];
                                    }
                                    break;

                                case 'max':

                                    if (strlen($_POST[$key]) > $minmax) {
                                        $this->message[$key] = $error['error'];
                                    }
                                    break;
                            }
                        }
                        break;
                    case 'is_select':

                        if ($_POST[$key] == '' || $_POST[$key] == 'switch') {
                            $this->message[$key] = $error;
                        }

                        break;
                }
            }
        }
    }

    /**
     * Displays validation error message for each field.
     * @param  string $value
     */
    public function display($value)
    {
        if (isset($_POST)) {
            if (isset($this->message[$value])) {
                echo '<span class="ValidationErrors">'.$this->message[$value].'</span>';
            }
        }
    }

    /**
     *
     * Leaves field filled if validation error accrued
     * @param string $item
     * @return string
     */
    public static function get($item)
    {
        if (isset($_POST[$item])) {
            return $_POST[$item];
        } elseif (isset($_GET[$item])) {
            return $_GET[$item];
        }
    }
}