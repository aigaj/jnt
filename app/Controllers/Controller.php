<?php

namespace App\Controllers;

/**
 * Initialising model
 */
class Controller
{

    function __construct()
    {
        $this->productsmodel = new \App\Models\ProductsModel;
    }
}