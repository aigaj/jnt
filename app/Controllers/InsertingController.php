<?php

namespace App\Controllers;

use App\Core\App;

/**
 * Sorting out data which needs to be inserted
 */
class InsertingController extends Controller
{

    /**
     * Upload data depending on type_switcher
     * @return string 
     */
    public function store()
    {
        $data = array(
            'SKU' => $_POST ['sku'],
            'name' => $_POST['name'],
            'price' => $_POST['price'],
            'type_switcher' => $_POST['switcher'],
        );
        switch ($_POST['switcher']) {
            case 'disk':
                $data ['size']   = $_POST['size'];
                break;
            case 'book':
                $data ['weight'] = $_POST['weight'];
                break;
            case 'furniture':
                $data ['lenght'] = $_POST['lenght'];
                $data ['height'] = $_POST['height'];
                $data ['width']  = $_POST['width'];
                break;
            default;
                break;
        }
        $this->productsmodel->storing($data);
        return redirect('products');
    }

    /**
     *
     * Defines rules for inputed data validation
     * after validation sends data to  store function to store validated data
     * @return string
     */
    public function index()
    {
        $form = new \App\Controllers\ValidationController();
        if (isset($_POST['submit'])) {
            $form->rules(array(
                'sku' => array(
                    'between' => array(
                        'min' => 9,
                        'max' => 9,
                        'error' => 'SKU must be 9 characters long ')),
                'name' => array(
                    'alphanumeric' => 'error',
                    'between' => array(
                        'min' => 1,
                        'max' => 100,
                        'error' => 'Name is required')),
                'price' => array(
                    'numeric' => 'Please enter valid price',
                    'between' => array(
                        'min' => 1,
                        'max' => 5,
                        'error' => 'Invalid price')),
                'switcher' => array(
                    'is_select' => 'Please select category')
            ));
            if ($_POST['switcher'] == 'disk') {
                $form->rules(array(
                    'size' => array(
                        'numeric' => 'Please enter valid size',
                        'between' => array(
                            'min' => 3,
                            'max' => 5,
                            'error' => 'Invalid size'))
                ));
            } elseif ($_POST['switcher'] == 'book') {
                $form->rules(array(
                    'weight' => array(
                        'numeric' => 'Please enter valid weight',
                        'between' => array(
                            'min' => 3,
                            'max' => 5,
                            'error' => 'Invalid weight'))
                    )
                );
            }
            elseif ($_POST['switcher'] == 'furniture') {
                $form->rules(array(
                    'height' => array(
                        'numeric' => 'Please enter valid weight',
                        'between' => array(
                            'min' => 3,
                            'max' => 5,
                            'error' => 'Invalid height')),
                    'width' => array(
                        'numeric' => 'Please enter valid weight',
                        'between' => array(
                            'min' => 3,
                            'max' => 5,
                            'error' => 'Invalid width')),
                    'lenght' => array(
                        'numeric' => 'Please enter valid weight',
                        'between' => array(
                            'min' => 3,
                            'max' => 5,
                            'error' => 'Invalid lenght'))
                    )
                );
            }

            if (empty($form->message)) {
                $validate = new \App\Controllers\InsertingController();
                $validate->store();
            }
        }

        $insert = 'inserting';
        return view('inserting', compact('insert', 'form'));
    }
}